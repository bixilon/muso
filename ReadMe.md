# Minecraft Update Services offline
You have many clients which want to update or install minecraft but only a slow internet connection?
**M**inecraft **U**pdate **S**ervices **o**ffline caches many big files on a central server.

## URLS to cache
- https://launchermeta.mojang.com/v1/packages
- https://launcher.mojang.com/v1/objects
- https://libraries.minecraft.net/
- https://resources.download.minecraft.net/

## Installation
1. Install this on a server (which has some space, depending on your users. 10+ GB recommended. Calculate 150 MB per Minecraft version)
BUT: You must have at least 500 MB storage. (Java, Launcher, Minecraft, Resources, etc)
2. Generate an SSL certificate for the urls above, install it on all clients and your server (e.g. GPOs)
3. Redirect (DNS level) all urls above to your server (via hosts file or DNS server)
4. Download Minecraft once to build a small cache (better: download all required versions on one client)

## Functionality
Because all requests are getting redirected to our own server, the server checks the requested url.
If the url matches the ones above, the server checks if the files exists. If it exists, the file is getting read and sent back to the client.
If not, the file is getting downloaded and then sent back. Easy :)

## Webserver requirements
- `.htaccess` functionality (In Apache this is disabled by default)
- SSL
- Read and write access to all the webroot
- mod rewrite
- php curl mod
- Optional: If your connection is very slow (< 1 Mbit), increase the `max_execution_time` in your php.ini, otherwise files might break, because the script gets killed and files are getting stored broken. -/-

## Practical use case?
Image having many clients in an active directory environment. And then the next step:
Mojang releases a new update of minecraft. The update has around 150 MB. If you have 100 clients,
you would need to download 15 GB of files (and all are the same!). If you have 100 Mbits Internet,
you would need about (15000 / (100 / 8) / 60) 21 minutes. But mojang servers are not always fast,
maybe you have only 10 Mbit throughput. This would be about 3h. That is way too long for every update!
You only need to set up this script once (about 20 minutes) and 1 download would take wost case 3 minutes (10 Mbits).
If you have a gigabit in your office, the deployment takes some seconds for every client. Instead of 3h it's just about 3 minutes.
You could even deploy and play minecraft with your colleagues if your internet connection fails :)
