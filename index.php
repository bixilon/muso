<?php

// we only accept the following urls, we are not a proxy server
$urls = array("launchermeta.mojang.com", "launcher.mojang.com", "libraries.minecraft.net", "launchercontent.mojang.com", "resources.download.minecraft.net");

$dataFolder = "data";

if (!in_array($_SERVER['HTTP_HOST'], $urls)) {
    http_response_code(400);
    die("Illegal request, we are not a proxy server!");
}

if ($_SERVER['HTTP_HOST'] == "launchermeta.mojang.com") {
    if (!stringStartsWith($_SERVER['REQUEST_URI'], "/v1/packages/")) {
        returnRealData();
    }
} elseif ($_SERVER['HTTP_HOST'] == "launcher.mojang.com") {
    if (!stringStartsWith($_SERVER['REQUEST_URI'], "/v1/objects/")) {
        returnRealData();
    }
}

// check if requested url is valid
if (strpos($_SERVER['REQUEST_URI'], '../') !== false) {
    // is this manipulated??? Let mojang handle this
    returnRealData();
}

// cache all urls:

$filePath = $dataFolder . "/" . $_SERVER['HTTP_HOST'] . "/" . str_replace("/", "_", $_SERVER['REQUEST_URI']);
//error_log("Requesting: ". $_SERVER['HTTP_HOST'] . "/" . $_SERVER['REQUEST_URI']);
createFolderIfNotPresent($dataFolder . "/" . $_SERVER['HTTP_HOST']);
if (!file_exists($filePath)) {
    download("https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]", $filePath);
}
$dataSize = filesize($filePath); //Size is not zero base
while ($dataSize == 0) {
    unlink($filePath);
    download("https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]", $filePath);
    $dataSize = filesize($filePath);
}

// thanks https://gist.github.com/fzerorubigd/3899077

$mime = 'application/otect-stream'; //Mime type of file. to begin download its better to use this.
$filename = basename($filePath); //Name of file, no path included

//2- Check for request, is the client support this method?
if (isset($_SERVER['HTTP_RANGE']) || isset($HTTP_SERVER_VARS['HTTP_RANGE'])) {
    $ranges_str = (isset($_SERVER['HTTP_RANGE'])) ? $_SERVER['HTTP_RANGE'] : $HTTP_SERVER_VARS['HTTP_RANGE'];
    $ranges_arr = explode('-', substr($ranges_str, strlen('bytes=')));
    //Now its time to check the ranges
    if ((intval($ranges_arr[0]) >= intval($ranges_arr[1]) && $ranges_arr[1] != "" && $ranges_arr[0] != "")
        || ($ranges_arr[1] == "" && $ranges_arr[0] == "")
    ) {
        //Just serve the file normally request is not valid :( 
        $ranges_arr[0] = 0;
        $ranges_arr[1] = $dataSize - 1;
    }
} else { //The client dose not request HTTP_RANGE so just use the entire file
    $ranges_arr[0] = 0;
    $ranges_arr[1] = $dataSize - 1;
}

//Now its time to serve file 
$file = fopen($filePath, 'rb');

$start = $stop = 0;
if ($ranges_arr[0] === "") { //No first range in array
    //Last n1 byte
    $stop = $dataSize - 1;
    $start = $dataSize - intval($ranges_arr[1]);
} elseif ($ranges_arr[1] === "") { //No last
    //first n0 byte
    $start = intval($ranges_arr[0]);
    $stop = $dataSize - 1;
} else {
    // n0 to n1
    $stop = intval($ranges_arr[1]);
    $start = intval($ranges_arr[0]);
}
//Make sure the range is correct by checking the file

fseek($file, $start, SEEK_SET);
$start = ftell($file);
fseek($file, $stop, SEEK_SET);
$stop = ftell($file);

$data_len = $stop - $start;

//Lets send headers 

if (isset($_SERVER['HTTP_RANGE']) || isset($HTTP_SERVER_VARS['HTTP_RANGE'])) {
    header('HTTP/1.0 206 Partial Content');
    header('Status: 206 Partial Content');
}
header('Accept-Ranges: bytes');
header('Content-type: ' . $mime);
header('Content-Disposition: attachment; filename="' . $filename . '"');
header("Content-Range: bytes $start-$stop/" . $dataSize);
header("Content-Length: " . ($data_len + 1));

//Finally serve data and done ~!
fseek($file, $start, SEEK_SET);
$bufferSize = 2048000;

ignore_user_abort(true);
@set_time_limit(0);
while (!(connection_aborted() || connection_status() == 1) && $data_len > 0) {
    echo fread($file, $bufferSize);
    $data_len -= $bufferSize;
    flush();
}

fclose($file);


// lib

function stringStartsWith($string, $prefix)
{
    return substr($string, 0, strlen($prefix)) === $prefix;
}

function returnRealData()
{
    global $targetURL;
    $targetURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    require_once "proxy.php";
    die();
}

function createFolderIfNotPresent($path)
{
    if (!file_exists($path)) {
        mkdir($path, 0777, true);
    }
}

// thanks https://stackoverflow.com/questions/13147985/optimizing-memory-usage-and-changing-file-contents-with-php
function download($file_source, $file_target)
{
    error_log("Downloading: " . $file_source);
    $rh = fopen($file_source, 'rb');
    $wh = fopen($file_target, 'w+b');
    if (!$rh || !$wh) {
        return false;
    }

    while (!feof($rh)) {
        if (fwrite($wh, fread($rh, 4096)) === FALSE) {
            return false;
        }
    }

    fclose($rh);
    fclose($wh);
    return true;
}
